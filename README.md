# Hold Me Tight

How I got engaged, or the most convoluted way of making an engagement ring.

A journey of learning, AI, tight deadlines, and, of course, love.

## Context

As a robotics engineer, I knew nothing about jewelry, or engagements, for that matter, before embarking on this process. I figured, "How hard can it be?". After all, it's simply manufacturing something, especially since I have over a year to figure this out.

Now I know how little I know about jewelry, or anything else.

## Design

I'm extremely opinionated. Which is not surprising given that I deal with software every day.

After researching engagement rings, I felt I wanted something different. But I couldn't quite put my finger on it. So, as any good engineer would, I started with requirements.

### Here Requirements

We had discussed the topic of marriage and rings many times. And I, with not so much subtlety, probed for the main qualities she'd like the ideal ring to have.

She picked up on my hints very quickly and made it very clear: rose gold, sapphire, delicate.

### My Requirements

Given these constraints and some further research, I concluded, "I really want to make this thing". In part to see if I could do it (ego), and in part because I really want this to be extra special.

Since I was captivated by the cool geometry of tension-set rings as opposed to the more common prong set, I decided this was going to be the guiding principle.

Making something beautiful is all nice and dandy, but making it also functional is that much more awesome. Since the purpose of this ring is to be worn forever, day in and day out, durability and comfort also need to be considered. And for some reason, I'm especially interested in making sure it's snag-free. Traditional designs look like they are going to get snagged everywhere, especially pockets and sweaters.

Of course, all this should be done using only open-source software.

### Coming Up With The Design

I'm by no means a designer, and my aesthetic sense certainly needs some help. Also, common tension set designs are anything but delicate. They rely on the thickness of the material to hold the stone.

But what I am is a stubborn engineer and nerd! A nerd with a [Midjourney](https://midjourney.com/) account!

I tried many prompts on Midjourney and some minor editing on Gimp until I got a reference picture I was happy with.

![tension-set rings by Midjourney](assets/img/midjourney_inspiration.png "Prompt: tension-set sapphire (rose-gold) engagement ring with diamonds --v 5")

The final edited inspiration image:

![edited tension-set rings by Midjourney](assets/img/sotilrac_tension-set_rose_gold_sapphire_engagement_ring_with_di_e7228dd7-12cf-4024-8549-8a6a6f91e757.png "Prompt: tension-set rose-gold sapphire engagement ring with diamonds --v 5"){width=50%}

## Jewelry CAD

It turns out making Jewelry CAD is an actual job.


## Timeline

| Date | Events |
| ---  | ---    |
| 2020 | We had early discussions about marriage, she would prefer to be proposed to in 2023. |
| 2021 | Understanding the ring requirements. At this point, I'm feeling great about how much time I have to work on this project. |
| ...<br><br>A long time<br><br>... | Procrastination, life, learning Blender and subdivision modeling, playing with Midjourney and ChatGPT. |
| 2023-05-24 | Design inspiration created with Midjourney. |
| 2023-08-05 | Jewelry CAD Job Posted on Upwork. |
| 2023-08-07 | Jewelry CAD contract started with two separate designers |
| ...<br><br>What felt like a long time<br><br>... | back-and-forth feedback and cross-pollinating inspirations for the designers. This went on mainly late at night after she would fall asleep, |
| 2023-08-12 | CAD Completed |
| 2023-08-12 16:42 | 3D Print ordered. After a lot of difficulty getting the credit card to work on Shapeways. |
| 2023-09-18 : 12:26 | The printed band is dropped on the porch. The stone fits flawlesly. It looks amazing, and the size is right! |
| 2023-09-19 : 13:30 | The ring parts are dropped off at the jeweler. |
| 2023-09-07 : 17:30 | The ring is ready at the jeweler |
| 2023-09-07 18:30 | I called the restaurant with a crazy request. I explained that I wanted to propose the next day at dinner. They were very excited, and we came up with a plan: bring in the ring inside a glass of champagne at the beginning of the meal. |
| 2023-09-08 | Her birthday, a.k.a. proposal day. |
| 2023-09-08 12:30 | Heading out to pick up the ring (40-min bike ride) |
| 2023-09-08 12:50 | Realizing the jeweler is not available until 14:00 |
| ...<br><br>Time for reflection<br><br>... | Since I had about an hour to kill, I decided to park the bike by the seaport, breathe and relax. I really enjoyed having this unexpected time to take stock of everything that led to this moment. It was a moment of serenity and excitement. |
| 2023-09-08 14:05 | I picked up the ring, it looks flawless. The "bar" fix looks great. It's much shinier than any of my renders. As a matter of fact, the rendered images would look fake if they were so shiny. |
| 2023-09-08 14:30 | I biked to the restaurant and called when I went there. No one answered, and I almost panicked because it looked very closed. Fortunately, the door was open when I pushed it and there was someone inside. I dropped the ring with them and asked if they could take a photo when it happens. |
| 2023-09-08 15:46 | I got a call from the restaurant manager about taking pictures. The plan was to discreetly give my phone to the host, so they could take pictures. |
| 2023-09-08 16:00 | I got back home from my "dentist appointment" just in time for some work meetings. |
| 2023-09-08 18:10 | Getting ready to go out for her birthday dinner date. |
| 2023-09-08 19:10 | We parked the car downtown. We have 35 minutes to kill before our reservations at 19:45. |
| 2023-09-08 19:30 | We decide we've walked enough, she's getting hungry,  and enter the restaurant |
| 2023-09-08 19:40 | She said *yes*! 🍾🎉🥰 |

